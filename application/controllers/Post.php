<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {
	
	public function __construct(){
 
        parent::__construct();
  		$this->load->model('frequent_model');
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$id = $this->uri->segment(2);
		if(!empty($id)){
			echo json_encode($this->frequent_model->select('total_number_of_comments','post'));
		}else{
			echo json_encode($this->frequent_model->select_option('post_id!=','','post','total_number_of_comments'));
		}
		
	}
	
	public function comment()
	{
		if($_POST){
			$type = $_POST['attribute'];
			$value = $_POST['value_type'];
			$data['comment'] = $this->frequent_model->select_option($type.'=',$value,'comments','');
		}else{
			$data['comment'] = $this->frequent_model->select('id_comment','comments');
		}
		
		$this->load->view('comment',$data);		
	}
}
