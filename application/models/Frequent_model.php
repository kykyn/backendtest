<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Frequent_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
	
	public function select($order,$table){
        $this->db->select();
        $this->db->from($table);
        $this->db->order_by($order,'desc');
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }
	
	//-- select by id
    function select_option($para,$id,$table,$order=null){
        $this->db->select();
        $this->db->from($table);
        $this->db->where($para, $id);
		(!empty($order))? $this->db->order_by($order,'desc') : '';
		if($table == ''){
			$this->db->order_by('created_at','DESC');
		}
        $query = $this->db->get();
        $query = $query->result_array();  
        return $query;
    }	
	
    //-- delete function
    function delete($id,$table){
        $this->db->delete($table, array('id' => $id));
        return;
    } 

    //-- delete option function
    function delete_option($var,$id,$table){
        $this->db->delete($table, array($var => $id));
        return;
    }
	
	 //-- update function
    function update_option($action, $para, $id, $table){
        $this->db->where($para,$id);
        $this->db->update($table,$action);
        return;
    }
	
	//-- insert function
	public function insert($data,$table){
        $this->db->insert($table,$data);        
        return $this->db->insert_id();
    }

    //-- edit function
    function edit_option($action, $id, $table){
        $this->db->where('id',$id);
        $this->db->update($table,$action);
        return;
    }
	
	//-- check exist
	public function check_exist($table,$var,$id){
        $this->db->select('*');
        $this->db->from($table);
        $this->db->where($var, $id); 
        $this->db->limit(1);
        $query = $this->db->get();
        if($query->num_rows() == 1) {                 
            return $query->result();
        }else{
            return false;
        }
    }
}